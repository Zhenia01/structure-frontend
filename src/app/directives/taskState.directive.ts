import {
  Directive,
  Input,
  ElementRef,
  OnInit,
  Renderer2,
  OnDestroy,
} from '@angular/core';
import { Task } from '../models/task/task';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TaskState } from '../models/task-state/task-state';
import { TaskStateService } from '../services/task-state.service';

@Directive({
  selector: '[appTaskState]',
})
export class TaskStateDirective implements OnInit, OnDestroy {
  @Input('appTaskState') task: Task = {} as Task;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private elRef: ElementRef,
    private renderer: Renderer2,
    private taskStateService: TaskStateService
  ) {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.taskStateService
      .getById(this.task.taskStateId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response) => {
          let taskState = (response.body as TaskState).value;
          switch (taskState) {
            case 'Finished': {
              this.renderer.addClass(this.elRef.nativeElement, 'finishedTask');
              break;
            }
            case 'Started': {
              this.renderer.addClass(this.elRef.nativeElement, 'startedTask');
              break;
            }
            case 'Created': {
              this.renderer.addClass(this.elRef.nativeElement, 'createdTask');
              break;
            }
            case 'Canceled': {
              this.renderer.addClass(this.elRef.nativeElement, 'canceledTask');
              break;
            }
          }
        },
        (error) => console.log(error)
      );
  }
}
