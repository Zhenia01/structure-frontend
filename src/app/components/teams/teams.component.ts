import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { Team } from 'src/app/models/team/team';
import { TeamService } from 'src/app/services/team.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();

  public displayedColumns: string[] = ['name', 'createdAt', 'actions'];

  public teams: Team[] = [];

  constructor(private teamService: TeamService) {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.getTeams();
  }

  public getTeams() {
    this.teamService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response) => this.teams = response.body as Team[],
        (error) => console.log(error)
      );
  }

  public deleteTeam(id: number) {
    this.teamService
      .deleteById(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => this.getTeams(),
        (error) => console.log(error)
      );
  }
}
