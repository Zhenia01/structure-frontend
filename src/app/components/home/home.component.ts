import { Component, OnInit } from '@angular/core';
import { TaskStateService } from '../../services/task-state.service';
import { TaskState } from 'src/app/models/task-state/task-state';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor(private taskStateService: TaskStateService) {}

  ngOnInit(): void {
  }
}
