import { Component, OnInit, OnDestroy } from '@angular/core';
import { Project } from 'src/app/models/project/project';
import { Subject } from 'rxjs';
import { ProjectService } from 'src/app/services/project.service';
import { takeUntil } from 'rxjs/operators';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { Task } from 'src/app/models/task/task';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class ProjectsComponent implements OnInit, OnDestroy {
  expandedElement: Project | null = null;

  private unsubscribe$ = new Subject<void>();

  public displayedColumns: string[] = [
    'name',
    'description',
    'createdAt',
    'deadline',
    'actions',
  ];

  public projects: Project[] = [];

  constructor(private projectService: ProjectService) {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.getProjects();
  }

  public getProjects() {
    this.projectService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response) => (this.projects = response.body as Project[]),
        (error) => console.log(error)
      );
  }

  public deleteProject(id: number) {
    this.projectService
      .deleteById(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => this.getProjects(),
        (error) => console.log(error)
      );
  }
}
