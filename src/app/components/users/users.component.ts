import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { User } from 'src/app/models/user/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  public displayedColumns: string[] = [
    'firstName',
    'lastName',
    'email',
    'birthday',
    'registeredAt',
    'actions',
  ];

  public users: User[] = [];

  constructor(private userService: UserService) {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.getUsers();
  }

  public getUsers() {
    this.userService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response) => (this.users = response.body as User[]),
        (error) => console.log(error)
      );
  }

  public deleteUser(id: number) {
    this.userService
      .deleteById(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => this.getUsers(),
        (error) => console.log(error)
      );
  }
}
