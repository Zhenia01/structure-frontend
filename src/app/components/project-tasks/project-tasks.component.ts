import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/models/task/task';
import { Project } from 'src/app/models/project/project';

@Component({
  selector: 'app-project-tasks',
  templateUrl: './project-tasks.component.html',
  styleUrls: ['./project-tasks.component.scss'],
})
export class ProjectTasksComponent implements OnInit, OnDestroy {

  @Input() project: Project = {} as Project;

  private unsubscribe$ = new Subject<void>();

  displayedColumns: string[] = ['name', 'description', 'createdAt', 'finishedAt', 'actions'];

  tasks: Task[] = [];

  constructor(private taskService: TaskService) {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.getTasks();
  }

  private getTasks() {
    this.taskService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (response) => this.tasks = (response.body as Task[]).filter(task => task.projectId === this.project.id),
        (error) => console.log(error)
      );
  }

  public deleteTask(id: number) {
    this.taskService
      .deleteById(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => this.getTasks(),
        (error) => console.log(error)
      );
  }
}
