import { Routes } from '@angular/router';
import {ProjectsComponent} from './components/projects/projects.component'
import {ProjectTasksComponent} from './components/project-tasks/project-tasks.component'
import {UsersComponent} from './components/users/users.component'
import {TeamsComponent} from './components/teams/teams.component'

export const AppRoutes: Routes = [
    { path: 'projects', component: ProjectsComponent, pathMatch: 'full' },
    { path: 'projectsTasks', component: ProjectTasksComponent, pathMatch: 'full' },
    { path: 'users', component: UsersComponent, pathMatch: 'full' },
    { path: 'teams', component: TeamsComponent, pathMatch: 'full' },
    { path: '**', redirectTo: '' }
];
