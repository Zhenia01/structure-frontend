import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ukrainianDate',
})
export class UkrainianDatePipe implements PipeTransform {
  transform(value: Date | string): string {
    let month: string = '';
    const date = new Date(value);
    switch (date.getMonth()) {
      case 0: {
        month = 'Січень';
        break;
      }
      case 1: {
        month = 'Лютий';
        break;
      }
      case 2: {
        month = 'Березень';
        break;
      }
      case 3: {
        month = 'Квітень';
        break;
      }
      case 4: {
        month = 'Травень';
        break;
      }
      case 5: {
        month = 'Червень';
        break;
      }
      case 6: {
        month = 'Липень';
        break;
      }
      case 7: {
        month = 'Серпень';
        break;
      }
      case 8: {
        month = 'Вересень';
        break;
      }
      case 9: {
        month = 'Жовтень';
        break;
      }
      case 10: {
        month = 'Листопад';
        break;
      }
      case 11: {
        month = 'Грудень';
        break;
      }
    }
    return `${date.getDay()+1}, ${month}, ${date.getFullYear()}`;
  }
}
