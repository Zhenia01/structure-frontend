import { Injectable } from '@angular/core';
import { User } from '../models/user/user';
import { BaseService } from './abstract/base.service';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root',
})
export class UserService extends BaseService<User> {
  public readonly routePrefix = 'api/users';

  constructor(protected readonly httpService: HttpInternalService) {
    super(httpService);
  }
}
