import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Project } from '../models/project/project';
import { BaseService } from './abstract/base.service';

@Injectable({
  providedIn: 'root',
})
export class ProjectService extends BaseService<Project> {
  public readonly routePrefix = 'api/projects';

  constructor(protected readonly httpService: HttpInternalService) {
    super(httpService);
  }
}
