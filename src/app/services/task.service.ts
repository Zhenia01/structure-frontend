import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Task } from '../models/task/task';
import { BaseService } from './abstract/base.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService extends BaseService<Task> {
  public readonly routePrefix = 'api/tasks';

  constructor(protected readonly httpService: HttpInternalService) {
    super(httpService);
  }
}
