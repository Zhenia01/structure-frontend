import { HttpInternalService } from '../http-internal.service';

export abstract class BaseService<T> {

  public abstract readonly routePrefix: string;

  constructor(protected readonly httpService: HttpInternalService) {}

  public getById(id: number) {
    return this.httpService.getRequest<T>(`${this.routePrefix}/${id}`);
  }

  public getAll() {
    return this.httpService.getRequest<T[]>(this.routePrefix);
  }

  public deleteById(id: number) {
    return this.httpService.deleteRequest<T>(`${this.routePrefix}/${id}`);
  }
}
