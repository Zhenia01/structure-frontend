import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Team } from '../models/team/team';
import { BaseService } from './abstract/base.service';

@Injectable({
  providedIn: 'root'
})
export class TeamService extends BaseService<Team> {
  public readonly routePrefix = 'api/teams';

  constructor(protected readonly httpService: HttpInternalService) {
    super(httpService);
  }
}
