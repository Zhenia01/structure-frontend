import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { TaskState } from '../models/task-state/task-state';
import { BaseService } from './abstract/base.service';

@Injectable({
  providedIn: 'root',
})
export class TaskStateService extends BaseService<TaskState> {
  public readonly routePrefix = 'api/taskStates';

  constructor(protected readonly httpService: HttpInternalService) {
    super(httpService);
  }
}
